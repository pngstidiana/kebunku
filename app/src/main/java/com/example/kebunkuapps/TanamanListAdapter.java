package com.example.kebunkuapps;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class TanamanListAdapter extends RecyclerView.Adapter<TanamanListAdapter.ListViewHolder> {
    private ArrayList<Tanaman> listTanaman;

    public TanamanListAdapter(ArrayList<Tanaman> list) {
        this.listTanaman = list;
    }

    private OnItemClickCallback onItemClickCallback;
    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row_tanaman, viewGroup, false);
        return new ListViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ListViewHolder holder, int position) {
        Tanaman tanaman = listTanaman.get(position);

        Glide.with(holder.itemView.getContext())
                .load(tanaman.getPhoto())
                .apply(new RequestOptions().override(55, 55))
                .into(holder.imgPhoto);
        holder.tvName.setText(tanaman.getName());
        holder.tvDetail.setText(tanaman.getDetail());
        holder.tvNamaLatin.setText(tanaman.getNamalatin());
        holder.tvKhasiat.setText(tanaman.getKhasiat());
        holder.tvPenjelasan.setText(tanaman.getPenjelasan());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickCallback.onItemClicked(listTanaman.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listTanaman.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPhoto;
        TextView tvName, tvDetail, tvNamaLatin, tvKhasiat, tvPenjelasan;
        ListViewHolder(View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.img_item_photo);
            tvName = itemView.findViewById(R.id.tv_item_name);
            tvDetail = itemView.findViewById(R.id.tv_item_detail);
            tvNamaLatin = itemView.findViewById(R.id.tv_namalatin);
            tvKhasiat = itemView.findViewById(R.id.tv_khasiat);
            tvPenjelasan = itemView.findViewById(R.id.tv_penjelasan);
        }
    }
    public interface OnItemClickCallback {
        void onItemClicked(Tanaman data);
    }
}
