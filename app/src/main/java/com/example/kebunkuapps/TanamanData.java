package com.example.kebunkuapps;

import java.util.ArrayList;

public class TanamanData {
    public static String[][] data = new String[][]{
            {"Cabai", "Media Tanah", "https://mesinpertanian.id/wp-content/uploads/2019/01/Kementan-Akan-Segera-Kembangkan-Cabai-Tahan-Hujan.jpg", "Capsicum Frutescens", " 1. Mengatasi Hidung Tersumbat\n 2. Meredakan Nyeri \n 3. Meningkatkan Imunitas Tubuh\n 4. Mengurangi risiko penyakit jantung \n 5. Mencegah penyakit kanker \n 6. Membakar lemak tubuh \n 7. Memperpanjang umur","Cabai atau cabai merah adalah buah dan tumbuhan anggota genus Capsicum. Buahnya dapat digolongkan sebagai sayuran maupun bumbu, tergantung bagaimana digunakan. Sebagai bumbu, buah cabai yang pedas sangat populer di Asia Tenggara sebagai penguat rasa makanan. Bagi seni masakan Padang, cabai bahkan dianggap sebagai bahan makanan pokok ke sepuluh (alih-alih sembilan)."},
            {"Pakcoy","Media Tanah","https://upload.wikimedia.org/wikipedia/commons/e/e3/Baby_Pak_Choi_%2801%29.JPG", "Brassica rapa subsp. chinensis"," 1. Mengandung rendah kalori \n 2. Membantu menjaga berat badan \n 3. Memiliki anti oksidan ", "Sayuran yang dikenal pula sebagai sawi sendok ini mudah dibudidayakan dan dapat dimakan segar (biasanya dilayukan dengan air panas) atau diolah menjadi asinan. Kadang-kadang sawi ini juga disebut sawi hijau karena fungsinya mirip, meskipun sawi sendok lebih kaku teksturnya serta ukurannya cenderung lebih kecil dan meroset.\n" +
                    "\n" +
                    "Jenis sayuran ini mudah tumbuh di dataran rendah maupun dataran tinggi. Bila ditanam pada suhu sejuk tumbuhan ini akan cepat berbunga. Karena biasanya dipanen seluruh bagian tubuhnya (kecuali akarnya), sifat ini kurang disukai. Pemuliaan sawi ditujukan salah satunya untuk mengurangi kepekaan akan suhu ini. Sayuran ini biasanya digunakan dalam bahan sup atau penghias makanan."},
            {"Strawberry","Media Tanah","https://pathsensors.com/wp-content/uploads/strawberries.jpg", "Fragaria × ananassa", " 1. Buah strawberry sebagai Anti-kanker \n 2. Menghilangkan jerawat \n 3. Mempertajam daya ingat serta \n     kesehatan mental \n 4. Mmenjaga kesehatan pencernaan \n 5. Baik untuk mata", "Strawberry merupakan buah yang tergolong pada beri-berian, namaun dari semua jenis beri-berian buah ini primadonanya. Strawberi diduga berasal dari persilangan antara Fragaria chiloensis yang berasal dari Chilie dan Fragaria virginana yang berasal dari Amerika Utara. Hasil silangan ini untuk pertama kali dibudidayakan di Inggris. Strawberi sudah dibudidayakan pada zaman Romawi kuno."},
            {"Bayam","Media Tanah dan Hidroponik","https://cdn.jitunews.com/dynamic/article/2015/05/11/13771/QSRP10iH1Q.JPG?w=630", "Amaranthus", " 1. Kaya akan zat besi \n 2. Bagus untuk pembentukan sel darah \n     merah \n 3. Membantu membentuk sistem imun \n 4. Meredakan gejala panas dalam", "Bayam adalah sejenis sayuran berdaun hijau atau merah. Bayam bertangkai lunak dan bisa dikonsumsi. Bayam memiliki bentuk daun yang bulat dan tangkai cukup panjang. Bayam banyak ditemui di rawa-rawa atau ditanam di tanah yang lembab. Bayam terdiri dari beberapa jenis. Varietas bayam yang terkenal adalah bayam hijau dan bayam merah. Bayam termasuk jenis sayuran yang direkomendasikan para ilmuwan karena memiliki kandungan zat besi yang tinggi."},
            {"Salada","Media Tanah dan Hidroponik","https://ecs7.tokopedia.net/img/cache/700/product-1/2016/3/10/3649264/3649264_b9a8f2be-5ce0-4425-ba80-6a6ead785cda.jpg", "Lactuca sativa" ," 1. Menjaga kesehatan jantung \n 2. Merawat Kecantikan kulit \n 3. Meningkatkan kekebalan tubuh \n 4. Mencegah komplikasi kehamilan \n 5. Menjaga kesehatan mata \n 6. Mecegah tulang keropos" ,"Selada adalah sejenis sayuran berdaun hijau maupun putih. Selada juga dikenal dengan nama sla. Selada umumnya dikonsumsi dalam keadaan mentah maupun matang. Selada memiliki karakteristik berupa daun yang bergelombang dan berwarna hijau atau putih serta rasa yang cenderung manis. Selada merupakan jenis sayuran yang mudah layu bila tidak disimpan dalam keadaan segar. Selada tumbuh di daerah tropis dan umumnya di dataran tinggi. Selada merupakan sayur dengan kandungan gizi yang tinggi dan aman dikonsumsi walaupun dalam keadaan mentah. Umumnya, selada digunakan pada makanan-makanan latin dan olahan salad."},
            {"Caisim Dakota","Media Hidroponik","https://i2.wp.com/resepkoki.id/wp-content/uploads/2018/02/sawi-hijau.jpg?fit=700%2C465&ssl=1", "Brassica juncea L." , " 1. Antioksidan \n 2. Detoksifikasi tubuh \n 3. Mencegah kanker \n 4. Menjaga sistem imun tubuh \n 5. Mengontrol kadar kolesterol \n 6. Baik untuk dikonsumsi ibu hamil \n 7. Membantu menurunkan berat badan", "Sawi memiliki banyak sekali jenis mulai dari jenis sawi hijau seperti caisim dan pakcoy, sawi pahit atau mustard greens, hingga sawi putih. Meskipun jenisnya berbeda-beda, tetapi kandungan nutrisi yang terkandung di dalamnya tidak jauh berbeda.\n" +
                    "\n" +
                    "Sawi merupakan salah satu jenis sayuran hijau yang baik bagi tubuh karena memiliki kandungan vitamin dan mineral yang dibutuhkan tubuh. Beberapa vitamin yang terkandung dalam sawi Antara lain seperti vitamin B kompleks dalam bentuk asam folat, vitamin A yang berasal dari karoten, vitamin C, dan vitamin K.\n" +
                    "\n" +
                    "Sawi juga memiliki kandungan berbagai mineral seperti sodium, zat besi, kalium, fosfor, dan kalsium. Sama seperti sayuran hijau lainnya, sawi juga memiliki kandungan serat yang cukup tinggi."},
            {"Kale","Media Hidroponik","https://img.rasset.ie/000a7649-800.jpg", "Brassica oleracea var. sabellica" , " 1. Kalsium lebih baik daripada susu \n 2. Menjaga kesehatan mata \n 3. Melindungi tekanan darah \n 4. Melindungi fungsi jantung \n 5. Mencegah kanker", "Kale adalah jenis sayuran yang berdaun hijau yang tergolong dalam keluarga kubis seperti brokoli, kembang kol, dan sawi. Seperti saudara-saudara yang lainnya, kale mengandung berbagai vitamin dan mineral yang baik untuk tubuh. Satu gelas kale mentah mengandung 33 kalori, 3 gram protein, 2,5 gram serat yang membantu proses pencernaan, vitamin A, vitamin C, vitamin K, asam folat dan vitamin B yang penting untuk perkembangan otak, asam alpha-linolenic dan lemak omega-3 yang baik untuk kesehatan jantung, lutein, zeaxantin, fosfor, kalium, kalsium, dan seng. Kale terdiri dari berbagai jenis, ada kale yang berdaun keriting, kale berdaun datar, atau bahkan kale yang berwarna hijau kebiruan dan masing-masing jenis kale mempunyai rasa yang berbeda-beda."},
    };


    public static ArrayList<Tanaman> getListData(){
        ArrayList<Tanaman> list = new ArrayList<>();
        for (String[] aData : data) {
            Tanaman tanaman = new Tanaman();
            tanaman.setName(aData[0]);
            tanaman.setDetail(aData[1]);
            tanaman.setPhoto(aData[2]);
            tanaman.setNamalatin(aData[3]);
            tanaman.setKhasiat(aData[4]);
            tanaman.setPenjelasan(aData[5]);
            list.add(tanaman);
        }
        return list;
    }
}
