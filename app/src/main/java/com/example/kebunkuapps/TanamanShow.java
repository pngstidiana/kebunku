package com.example.kebunkuapps;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.uncopt.android.widget.text.justify.JustifiedTextView;

public class TanamanShow extends Activity {
    ImageView imageView;
    TextView name, detail, namalatin, khasiat;
    String namep,detailp, namalatinp, khasiatp, penjelasanp;
    JustifiedTextView penjelasan;
    String imagep;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tanaman_show);

        imageView = findViewById(R.id.img_item_photo);
        name = findViewById(R.id.tv_item_name);
        detail = findViewById(R.id.tv_item_detail);
        namalatin = findViewById(R.id.tv_namalatin);
        khasiat = findViewById(R.id.tv_khasiat);
        penjelasan = (JustifiedTextView) findViewById(R.id.tv_penjelasan);

        namep = getIntent().getStringExtra("name");
        detailp = getIntent().getStringExtra("detail");
        imagep = getIntent().getStringExtra("image");
        namalatinp = getIntent().getStringExtra("namalatin");
        khasiatp = getIntent().getStringExtra("khasiat");
        penjelasanp = getIntent().getStringExtra("penjelasan");

        Glide.with(this)
                .load(imagep)
                .into(imageView);

        name.setText(namep);
        detail.setText(detailp);
        namalatin.setText(namalatinp);
        khasiat.setText(khasiatp);
        penjelasan.setText(penjelasanp);
    }
}