package com.example.kebunkuapps;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class Beli extends Activity {

    private RecyclerView rvTanaman;
    private ArrayList<Tanaman> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beli);

        rvTanaman = findViewById(R.id.rv_tanaman);
        rvTanaman.setHasFixedSize(true);

        list.addAll(TanamanData.getListData());
        showRecyclerCardView();
    }


    private void showRecyclerCardView(){
        rvTanaman.setLayoutManager(new LinearLayoutManager(this));
        CardViewTanamanAdapter cardViewTanamanAdapter = new CardViewTanamanAdapter(list);
        rvTanaman.setAdapter(cardViewTanamanAdapter);

    }
}
