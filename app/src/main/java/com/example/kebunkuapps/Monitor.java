package com.example.kebunkuapps;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import id.co.telkom.iot.AntaresHTTPAPI;
import id.co.telkom.iot.AntaresResponse;

import android.net.Uri;
import android.widget.MediaController;
import android.widget.VideoView;

public class Monitor extends Activity {

    private ImageButton btn_tanaman_tanah;
    private ImageButton btn_tanaman_hidroponik;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitor);

        btn_tanaman_tanah = findViewById(R.id.btn_tanaman_tanah);
        btn_tanaman_hidroponik = findViewById(R.id.btn_tanaman_hidroponik);

        btn_tanaman_tanah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Monitor.this, Monitor_Tanah.class));
            }
        });

        btn_tanaman_hidroponik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Monitor.this, Monitor_Hidroponik.class));
            }
        });
    }
}