package com.example.kebunkuapps;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.co.telkom.iot.AntaresHTTPAPI;
import id.co.telkom.iot.AntaresResponse;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;

public class Monitor_Hidroponik extends Activity implements AntaresHTTPAPI.OnResponseListener{

    LineChartView lineChartView;
    String[] axisData = {"M1", "M2", "M3", "M4", "M5", "M6", "M7", "M8", "M9",
            "M10"};
    int[] yAxisData = {50, 20, 15, 30, 20, 60, 15, 40, 45, 10};

    private Button btnRefresh1;
    private ImageButton streaming;

    private TextView txtDataHidroponik1, txtDataHidroponik2, txtDataHidroponik3, txtDataHidroponik4;
    private String TAG = "ANTARES-API";
    private AntaresHTTPAPI antaresAPIHTTP;
    private String dataDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitor__hidroponik);

        lineChartView = findViewById(R.id.chart);

        List yAxisValues = new ArrayList();
        List axisValues = new ArrayList();


        Line line = new Line(yAxisValues).setColor(Color.parseColor("#9C27B0"));

        for (int i = 0; i < axisData.length; i++) {
            axisValues.add(i, new AxisValue(i).setLabel(axisData[i]));
        }

        for (int i = 0; i < yAxisData.length; i++) {
            yAxisValues.add(new PointValue(i, yAxisData[i]));
        }

        List lines = new ArrayList();
        lines.add(line);

        LineChartData data = new LineChartData();
        data.setLines(lines);

        Axis axis = new Axis();
        axis.setValues(axisValues);
        axis.setTextSize(14);
        axis.setTextColor(Color.parseColor("#03A9F4"));
        data.setAxisXBottom(axis);

        Axis yAxis = new Axis();
        yAxis.setName("Dummy Data");
        yAxis.setTextColor(Color.parseColor("#03A9F4"));
        yAxis.setTextSize(14);
        data.setAxisYLeft(yAxis);

        lineChartView.setLineChartData(data);
        Viewport viewport = new Viewport(lineChartView.getMaximumViewport());
        viewport.top = 110;
        lineChartView.setMaximumViewport(viewport);
        lineChartView.setCurrentViewport(viewport);

        btnRefresh1 = (Button) findViewById(R.id.btnRefresh1);
        streaming = findViewById(R.id.btn_liveHidroponik);

        txtDataHidroponik1 = (TextView) findViewById(R.id.txtDataHi1);
        txtDataHidroponik2 = (TextView) findViewById(R.id.txtDataHi2);
        txtDataHidroponik3 = (TextView) findViewById(R.id.txtDataHi3);
        txtDataHidroponik4 = (TextView) findViewById(R.id.txtDataHi4);

        // --- Inisialisasi API Antares --- //
        //antaresAPIHTTP = AntaresHTTPAPI.getInstance();
        antaresAPIHTTP = new AntaresHTTPAPI();
        antaresAPIHTTP.addListener(this);

        streaming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Monitor_Hidroponik.this, Camera.class));
            }
        });

        btnRefresh1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                antaresAPIHTTP.getLatestDataofDevice("0609782c836378af:7101db18df57602b","KebunKu","Hidroponik");

            }
        });

    }

    @Override
    public void onResponse(AntaresResponse antaresResponse) {
        // --- Cetak hasil yang didapat dari ANTARES ke System Log --- //
        //Log.d(TAG,antaresResponse.toString());
        Log.d(TAG,Integer.toString(antaresResponse.getRequestCode()));
        if(antaresResponse.getRequestCode()==0){
            try {
                JSONObject body = new JSONObject(antaresResponse.getBody());
                dataDevice = body.getJSONObject("m2m:cin").getString("con");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        String s = dataDevice;
                        String parts[] = s.split(":");

                        //txtDataHidroponik1.setText(dataDevice);

                        txtDataHidroponik1.setText(parts[1].substring(0,3));
                        txtDataHidroponik2.setText(parts[2].substring(0,5));
                        txtDataHidroponik3.setText(parts[3].substring(0,4)+" cm");
                        txtDataHidroponik4.setText(parts[4]+" ^C");
                    }
                });
                Log.d(TAG,dataDevice);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
