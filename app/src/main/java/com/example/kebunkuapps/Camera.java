package com.example.kebunkuapps;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Camera extends Activity {

    private TextView waktu;
    private String thn,bln,tgl,jam,menit, menit2, amp;

    private Integer jam2, tgl2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        waktu = findViewById(R.id.waktu);

        SimpleDateFormat sdf = new SimpleDateFormat(":yyyy:MM:dd:HH:mm z");
        String currentDateandTime = sdf.format(new Date());
        waktu.setText("WAKTU "+currentDateandTime);

        String s = currentDateandTime;
        String parts[] = s.split(":");

        //txtDataHidroponik1.setText(dataDevice);

        //txtDataHidroponik1.setText(parts[1].substring(0,3));

        thn = parts[1].substring(0,4);
        bln = parts[2].substring(0,2);
        tgl = parts[3].substring(0,2);
        jam = parts[4].substring(0,2);
        menit = parts[5].substring(0,1);


        if ( Integer.parseInt(jam) < 07){
            tgl2 = Integer.parseInt(tgl) - 1;
            tgl = String.valueOf(tgl2);
            jam = "05";
            amp = "PM";
            menit2 = "1";
        }
        if (Integer.parseInt(jam) >= 07){
            tgl2 = Integer.parseInt(tgl);
            jam2 = Integer.parseInt(jam);
            jam = String.valueOf(jam2);
            amp = "AM";
            menit2 = "1";
        }
        if (Integer.parseInt(jam) > 12){
            tgl2 = Integer.parseInt(tgl);
            jam2 = Integer.parseInt(jam) - 12;
            jam = "0"+String.valueOf(jam2);
            amp = "PM";
            menit2 = "1";
        }
        if (Integer.parseInt(jam) > 18){
            tgl2 = Integer.parseInt(tgl);
            jam = "05";
            amp = "PM";
            menit2 = "1";
        }

        /*if (Integer.parseInt(jam) > 12){
            jam2 = Integer.parseInt(jam) - 12;
            jam = "0"+jam2;
            amp = "PM";
            menit2 = "1";
        } else if ( jam == "00"){
            jam = "05";
            amp = "PM";
            menit2 = "1";
        } else if (Integer.parseInt(jam) < 12){
            if (Integer.parseInt(jam) == 11){
                jam2 = Integer.parseInt(jam);
                jam = String.valueOf(jam2);
                amp = "AM";
                menit2 = "1";
            }
            if (Integer.parseInt(jam) == 10){
                jam2 = Integer.parseInt(jam);
                jam = String.valueOf(jam2);
                amp = "AM";
                menit2 = "1";
            }
            else {
                jam2 = Integer.parseInt(jam);
                jam = "0" + String.valueOf(jam2);
                amp = "AM";
                menit2 = "1";
            }
        } */

        String url1 = "https://labatr.id/kebunku/image/imgcron/"+thn+"_"+bln+"_"+tgl2+"/cam3_"+thn+"_"+bln+"_"+tgl2+"_"+jam+"_"+menit+menit2+"_"+amp+".jpg";

        new DownloadImageFromInternet((ImageView) findViewById(R.id.imageView6))
                .execute(url1);

        String url2 = "https://labatr.id/kebunku/image/imgcron/"+thn+"_"+bln+"_"+tgl2+"/cam4_"+thn+"_"+bln+"_"+tgl2+"_"+jam+"_"+menit+menit2+"_"+amp+".jpg";

        new DownloadImageFromInternet((ImageView) findViewById(R.id.imageView14))
                .execute(url2);
    }

    private class DownloadImageFromInternet extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;

        public DownloadImageFromInternet(ImageView imageView) {
            this.imageView = imageView;
            Toast.makeText(getApplicationContext(), "Please wait, it may take a few minute...", Toast.LENGTH_SHORT).show();
        }

        protected Bitmap doInBackground(String... urls) {
            String imageURL = urls[0];
            Bitmap bimage = null;
            try {
                InputStream in = new java.net.URL(imageURL).openStream();
                bimage = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                Log.e("Error Message", e.getMessage());
                e.printStackTrace();
            }
            return bimage;
        }

        protected void onPostExecute(Bitmap result) {
            imageView.setImageBitmap(result);
        }
    }

}