package com.example.kebunkuapps;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.uncopt.android.widget.text.justify.JustifiedTextView;

public class CaraTanah extends Activity {

    JustifiedTextView caraTanah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cara_tanah);

        caraTanah = (JustifiedTextView) findViewById(R.id.tv_caraTanah);
    }
}
