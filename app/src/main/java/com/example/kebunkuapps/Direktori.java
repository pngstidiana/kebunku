package com.example.kebunkuapps;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

public class Direktori extends Activity {
    private RecyclerView rvPlants;
    private Button btnTanah, btnHidroponik, btnHama;
    private ArrayList<Tanaman> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direktori);

        rvPlants = findViewById(R.id.rv_plant);
        rvPlants.setHasFixedSize(true);

        list.addAll(TanamanData.getListData());
        showRecyclerList();

        btnTanah = findViewById(R.id.bttn_tanah);
        btnHidroponik = findViewById(R.id.bttn_hidroponik);
        btnHama = findViewById(R.id.bttn_hama);

        btnTanah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Direktori.this, CaraTanah.class));
            }
        });
        btnHidroponik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Direktori.this, CaraHidroponik.class));
            }
        });
        btnHama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Direktori.this, Hama.class));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    private void showRecyclerList() {
        rvPlants.setLayoutManager(new LinearLayoutManager(this));
        TanamanListAdapter tanamanListAdapter = new TanamanListAdapter(list);
        rvPlants.setAdapter(tanamanListAdapter);

        tanamanListAdapter.setOnItemClickCallback(new TanamanListAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(Tanaman data) {
                showSelectedPlant(data);
            }
        });
    }

    private void showSelectedPlant(Tanaman tanaman) {
        Toast.makeText(this, "Kamu memilih " + tanaman.getName(), Toast.LENGTH_SHORT).show();
        Intent moveIntent = new Intent(Direktori.this, TanamanShow.class);

        moveIntent.putExtra("image", tanaman.getPhoto());
        moveIntent.putExtra("name", tanaman.getName());
        moveIntent.putExtra("detail", tanaman.getDetail());
        moveIntent.putExtra("namalatin", tanaman.getNamalatin());
        moveIntent.putExtra("khasiat", tanaman.getKhasiat());
        moveIntent.putExtra("penjelasan", tanaman.getPenjelasan());
        startActivity(moveIntent);
    }
}
