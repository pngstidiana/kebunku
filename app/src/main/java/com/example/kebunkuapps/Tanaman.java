package com.example.kebunkuapps;

public class Tanaman {
    private String name;
    private String detail;
    private String photo;
    private String namalatin;
    private String khasiat;
    private String penjelasan;

    public String getNamalatin() {
        return namalatin;
    }

    public void setNamalatin(String namalatin) {
        this.namalatin = namalatin;
    }

    public String getKhasiat() {
        return khasiat;
    }

    public void setKhasiat(String khasiat) {
        this.khasiat = khasiat;
    }

    public String getPenjelasan() {
        return penjelasan;
    }

    public void setPenjelasan(String penjelasan) {
        this.penjelasan = penjelasan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
