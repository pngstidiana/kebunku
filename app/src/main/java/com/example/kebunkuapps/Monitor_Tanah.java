package com.example.kebunkuapps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import id.co.telkom.iot.AntaresHTTPAPI;
import id.co.telkom.iot.AntaresResponse;

public class Monitor_Tanah extends Activity implements AntaresHTTPAPI.OnResponseListener{

    private Button btnRefresh;
    private ImageButton streaming1;

    private TextView txtDataH1, txtDataH2, txtDataH3, txtDataSuhu, txtDataPh;
    private String TAG = "ANTARES-API";
    private AntaresHTTPAPI antaresAPIHTTP;
    private String dataDevice;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitor__tanah);

        // --- Inisialisasi UI yang digunakan di aplikasi --- //
        btnRefresh = (Button) findViewById(R.id.btnRefresh1);
        streaming1 = findViewById(R.id.btn_liveTanah);

        txtDataH1 = (TextView) findViewById(R.id.txtDataH1);
        txtDataH2 = findViewById(R.id.txtDataH2);
        txtDataH3 = findViewById(R.id.txtDataH3);
        txtDataSuhu = findViewById(R.id.txtDataSuhu);
        txtDataPh = findViewById(R.id.txtDataPh);
        // --- Inisialisasi API Antares --- //
        //antaresAPIHTTP = AntaresHTTPAPI.getInstance();
        antaresAPIHTTP = new AntaresHTTPAPI();
        antaresAPIHTTP.addListener(this);

        streaming1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Monitor_Tanah.this, Camera.class));
            }
        });

        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                antaresAPIHTTP.getLatestDataofDevice("0609782c836378af:7101db18df57602b","KebunKu","PH-tanah");

            }
        });

    }

    @Override
    public void onResponse(AntaresResponse antaresResponse) {
        // --- Cetak hasil yang didapat dari ANTARES ke System Log --- //
        //Log.d(TAG,antaresResponse.toString());
        Log.d(TAG,Integer.toString(antaresResponse.getRequestCode()));
        if(antaresResponse.getRequestCode()==0){
            try {
                JSONObject body = new JSONObject(antaresResponse.getBody());
                dataDevice = body.getJSONObject("m2m:cin").getString("con");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //txtData.setText(dataDevice);

                        String s = dataDevice;
                        String parts[] = s.split(":");

                        txtDataH1.setText(parts[1].substring(0,5)+" %");
                        txtDataH2.setText(parts[2].substring(1,6)+" %");
                        txtDataH3.setText(parts[3].substring(1,6)+" %");
                        txtDataSuhu.setText(parts[4].substring(0,2)+" ^C");
                        txtDataPh.setText(parts[5]);
                    }
                });
                Log.d(TAG,dataDevice);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}