package com.example.kebunkuapps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Home extends Activity {
    private ImageButton btnBeli, btnKomunitas, btnMonitor, btnTanyaAhli;
    private ImageButton btnDirektori;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btnBeli = findViewById(R.id.bttn_beli_online);
        btnDirektori = findViewById(R.id.bttn_direktori);
        btnKomunitas = findViewById(R.id.bttn_komunitas);
        btnMonitor = findViewById(R.id.btn_tanaman_tanah);
        btnTanyaAhli = findViewById(R.id.bttn_tanya_ahli);

    btnBeli.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(Home.this, Beli.class));
        }
    });

    btnDirektori.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(Home.this, Direktori.class));
        }
    });

    btnKomunitas.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(Home.this, Komunitas.class));
        }
    });

    btnMonitor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Home.this, Monitor.class));
        }
    });

    btnTanyaAhli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Home.this, TanyaAhli.class));
            }
    });

    }
}
